<?php
/*
	RedisCache.php - Redis cache middleware for Slim framework
	Copyright 2015 abouvier <abouvier@student.42.fr>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

namespace Slim\Middleware;

use \Predis\ClientInterface;

class RedisRatelimiter extends \Slim\Middleware
{
	protected $client;
	protected $settings;

	public function __construct(ClientInterface $client, array $settings = [])
	{
		$this->client = $client;
		$this->settings = $settings;
	}

	public function call()
	{
		$app = $this->app;

        $totKey=$this->settings['prefix'].$app->request->getIp();
        $value = $this->client->get($totKey);
        if(isset($value)){
            $this->client->INCR($totKey);
        } else {
            $this->client->set($totKey, 1);
            $this->client->expire($totKey, $this->settings['timeout']);

        }

		if ($value > $this->settings['limit']) {
            $app->response->setBody("ratelimit exceeded");
			return;
		}

		$this->next->call();
	}
}
